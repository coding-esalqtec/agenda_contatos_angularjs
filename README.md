#Agenda de Contatos

##Sobre
Projeto de Agenda de contatos feito em AngularJS.

##Entidades
Este projeto inclui as entidades:  

 - Usuário  
 - Contato (Usuário possui vários contatos);  
 - Telefone (Contato possui vários telefones).

##Telas planejadas
As telas planejadas são:

 - Login;
 - Cadastro de usuário;
 - Edição do usuário (Tela "Meu Perfil");
 - Lista de contatos do usuário;
 - Formulário de adicionar/editar contato (e telefones).
 
##Backend
O backend é feito inteiramente com arquitetura REST e não é o foco do estudo, sendo apenas fornecido para ser consumido pelo frontend.